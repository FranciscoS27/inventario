<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<link rel="stylesheet" href="../inventario/public/css/formularios.css">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Especificaciones</title>
</head>
<body>
    <section id='contenido' class='col-xs-12 col-sm-12 col-md-12'>
        <!--Navegador-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-bottom: 20px; padding:0 2%">
            <div class="container-fluid">
                <a class="navbar-brand" href=""><h2>Inventario</h2></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">       
                        <li class="nav-item dropdown">
                            <a class="nav-link link" href="../inventario/?controller=user&action=vistaRegistrar">
                                Registro de productos
                            </a>                            
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link link" href="../inventario/?controller=user&action=vistaEditar">
                                Edicion de productos
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link link" href="../inventario/?controller=user&action=producto">
                                Especificaciones de productos
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link link" href="../inventario/?controller=user&action=vistaCambioCantidad">
                                Compra-Venta de productos
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <?php
            //Consultas de fecha y todos los productos en la BD
            use Modelos\Producto;            
            $productos = Producto::consultarTodo();
            $fecha = Producto::fecha();

            //Aviso en caso de busqueda mal hecha
            if(isset($_GET['failure'])){
                echo"<center><h2>Producto no seleccionado</h2></center>";
            }

            //Formulario para mostrar datos de los productos listados por la consulta de la BD
            echo"<form method='POST' action='../inventario/?controller=user&action=producto'> 
                <div class='dropdown'>
                    <select name='idProducto' class='center' required>
                        <option value='0'>Seleccione producto</option>";
                        while ($valores = mysqli_fetch_array($productos)) {
                            echo "<option value=$valores[id]>$valores[id].-$valores[nombre]</option>";
                        }
                    echo"</select>
                </div>
                <button type='submit' class='btn btn-primary center'>Buscar</button>
            </form>";

            //Verificar que ya se seleccionó un producto a visualizar
            if(isset($_POST['idProducto'])){
                if($_POST['idProducto'] > 0){
                    //Consulta de un unico producto
                    $producto = Producto::consultar($_POST['idProducto']);

                    //Formulario pre-llenado con los datos de la consulta del producto
                    echo"<div>
                        <div class='from-grup'>
                            <label>Nombre</label>
                            <input type='text' class='form-control' name='Nombre-producto' placeholder='' value='$producto->nombre' disabled>
                        </div>
                        <div class='from-grup'>
                            <label>Caducidad</label>
                            <input type='text' class='form-control' name='dia-caducidad' placeholder='Dia' value='$producto->caducidadD' disabled>
                            <input type='text' class='form-control' name='mes-caducidad' placeholder='Mes' value='$producto->caducidadM' disabled>
                            <input type='text' class='form-control' name='año-caducidad' placeholder='Año' value='$producto->caducidadA' disabled>
                        </div>
                        <div class='from-grup'>
                            <label>Cantidad</label>
                            <input type='text' class='form-control' name='cantidad' placeholder='' value='$producto->cantidad' disabled>
                        </div>
                        <div class='from-grup'>
                            <label>Costo</label>
                            <input type='text' class='form-control' name='costo' placeholder='' value='$producto->costo' disabled>
                        </div>
                    </div>";

                    //Aviso de caducidad proxima
                    if(($producto->caducidadA == $fecha['year'] && $producto->caducidadM == $fecha['mon'] && ((($producto->caducidadD)-($fecha['mday'])) < 5)) || ($producto->caducidadM < $fecha['mon'] && $producto->caducidadA == $fecha['year']) || $producto->caducidadA < $fecha['year']){
                        echo"<div class='from-grup'>
                            <small id='anuncio-caducidad' class='form-text tRojo'>El producto caduca en menos de 5 dias.</small>
                        </div>";
                    }


                    //Aviso de cantidades escasas
                    if($producto->cantidad < 5){
                        echo"<div class='from-grup'>
                            <small id='anuncio-existencia' class='form-text tRojo'>El producto tiene menos de 5 existencias.</small>
                        </div>";
                    }
                }
                //Redireccion por consulta mal hecha
                else{
                    header("location:../inventario/?controller=user&action=producto&failure");
                }
            }
        ?>
    </section>    
</body>
</html>