<!DOCTYPE html>
<html lang="en">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<link rel="stylesheet" href="../inventario/public/css/formularios.css">    
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro de productos</title>
</head>
<body>
    <section id='contenido' class='col-xs-12 col-sm-12 col-md-12'>
        <!--Navegador-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-bottom: 20px; padding:0 2%">
            <div class="container-fluid">
                <a class="navbar-brand" href=""><h2>Inventario</h2></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">       
                        <li class="nav-item dropdown">
                            <a class="nav-link link" href="../inventario/?controller=user&action=vistaRegistrar">
                                Registro de productos
                            </a>                            
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link link" href="../inventario/?controller=user&action=vistaEditar">
                                Edicion de productos
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link link" href="../inventario/?controller=user&action=producto">
                                Especificaciones de productos
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link link" href="../inventario/?controller=user&action=vistaCambioCantidad">
                                Compra-Venta de productos
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        
        <?php
            //Aviso de consulta mal hecha de producto unico
            if(isset($_GET['failure'])){
                echo"<center><h2 class='tRojo'>Producto no seleccionado</h2></center>";
            }
        ?>
        
        <!--Formulario para registrar producto-->
        <form method='POST' action='../inventario/?controller=user&action=registrar'>
            <div class='from-grup'>
                <label>Nombre</label>
                <input type='text' class='form-control' name='nombre' placeholder='Nombre del producto' value='' required>
            </div>
            <div class='from-grup'>
                <label>Caducidad</label>
                <input type='number' class='form-control' name='caducidadD' max='31' min='1' placeholder='Dia' value='' required>
                <input type='number' class='form-control' name='caducidadM' max='12' min='1' placeholder='Mes' value='' required>
                <input type='number' class='form-control' name='caducidadA' min='2021' max='2100' placeholder='Año' value='' required>
            </div>
            <div class='from-grup'>
                <label>Cantidad</label>
                <input type='number' class='form-control' min='0' name='cantidad' placeholder='Cantidad inicial de producto' value='0' required>
            </div>
            <div class='from-grup'>
                <label>Costo</label>
                <input type='text' class='form-control' name='costo' placeholder='Costo de producto' value='' required>
            </div>
            <input type='submit' class='btn btn-primary' value='Registrar'>
        </form>
    </section>    
</body>
</html>