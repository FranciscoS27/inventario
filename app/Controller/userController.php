<?php
    include "app/Modelos/conexion.php";
    include "app/Modelos/producto.php";
    use Modelos\Conexion;
    use Modelos\Producto;

    class userController{

        //Funcion para vista general de producto
        public function producto(){
            require "app/Views/vista-producto.php";
        }

        //Funcion para vista de registro de producto
        public function vistaRegistrar(){
            require "app/Views/registro-producto.php";
        }

        //Funcion para registrar producto en BD
        public function registrar(){            
            if(isset($_POST['nombre'])){
                $producto = new Producto;
                $producto->crear();
                require "app/Views/vista-producto.php";
            }
            else{
                header("location:../inventario/?controller=user&action=vistaRegistrar&failure");
            }
        }

        //Funcion para vista de editar producto
        public function vistaEditar(){
            require "app/Views/edicion-producto.php";
        }

        //Funcion de edicion de producto en BD
        public function editar(){            
            if(isset($_POST['idProducto'])){
                $producto = new Producto;
                $producto->editar();
                require "app/Views/vista-producto.php";
            }
            else{
                header("location:../inventario/?controller=user&action=vistaEditar&failure");
            }
        }

        //Funcion para eliminar producto en BD
        public function eliminar(){
            if(isset($_POST['idProducto'])){
                $producto = new Producto;
                $producto->eliminar($_POST['idProducto']);
                header("location:../inventario/?controller=user&action=producto");
            }
            else{
                header("location:../inventario/?controller=user&action=vistaEditar&failure");
            }
        }

        //Funcion para vista de compra-venta
        public function vistaCambioCantidad(){
            require "app/Views/existencias.php";
        }

        //Funcion para edicion de compra-venta de producto en BD
        public function cambioCantidad(){            
            if(isset($_POST['idProducto'])){
                $producto = new Producto;
                $producto->cambioCantidad();
                require "app/Views/vista-producto.php";
            }
            else{
                header("location:../inventario/?controller=user&action=vistaCambioCantidad&failure");
            }
        }

    }
?>