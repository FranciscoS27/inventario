<?php

    namespace Modelos;

    class Producto extends Conexion{
        public $id;
        public $nombre;
        public $cantidad;
        public $caducidadD;
        public $caducidadM;
        public $caducidadA;
        public $costo;

        public function __contruct(){
            parent::__construct();
        }

        //Llenado de atributos por metodo POST para su uso en cada funcion siguiente
        public function llenadoPost(){
            if(isset($_POST['idProducto'])){
                $this->id = $_POST['idProducto'];
            }
            if(isset($_POST['nombre'])){
                $this->nombre = $_POST['nombre'];
            }
            if(isset($_POST['caducidadD']) && isset($_POST['caducidadM']) && isset($_POST['caducidadA'])){
                $this->caducidadD = $_POST['caducidadD'];
                $this->caducidadM = $_POST['caducidadM'];
                $this->caducidadA = $_POST['caducidadA'];
            }
            if(isset($_POST['cantidad'])){
                $this->cantidad = $_POST['cantidad'];
            }
            if(isset($_POST['costo'])){
                $this->costo = $_POST['costo'];
            }
        }

        public static function consultar($idComponente){
            $conexion = new Conexion;
            $pre = mysqli_prepare($conexion->con, "SELECT* FROM productos WHERE id = ?");
            $pre->bind_param("i", $idComponente);
            $pre->execute();
            $resultado = $pre->get_result();
            return $resultado->fetch_object();
        }

        public static function consultarTodo(){
            $conexion = new Conexion;
            $pre = mysqli_prepare($conexion->con, "SELECT* FROM productos");
            $pre->execute();
            $resultado = $pre->get_result();
            return $resultado;
        }

        public function crear(){
            $this->llenadoPost();
            $pre = mysqli_prepare($this->con, "INSERT INTO productos(nombre, caducidadD, caducidadM, caducidadA, cantidad, costo) VALUES (?, ?, ?, ?, ?, ?)");
            $pre->bind_param("ssssss", $this->nombre, $this->caducidadD, $this->caducidadM, $this->caducidadA, $this->cantidad, $this->costo);
            $pre->execute();
        }

        public function editar(){
            $this->llenadoPost();
            $pre = mysqli_prepare($this->con, "UPDATE productos SET nombre = ?, caducidadD = ?, caducidadM = ?, caducidadA = ?, cantidad = ?, costo = ? WHERE id = ?");
            $pre->bind_param("sssssss", $this->nombre, $this->caducidadD, $this->caducidadM, $this->caducidadA, $this->cantidad, $this->costo, $this->id);
            $pre->execute();
        }

        public function eliminar($idProducto){
            $pre = mysqli_prepare($this->con, "DELETE FROM productos WHERE id = ?");
            $pre->bind_param("s", $idProducto);
            $pre->execute();
        }

        public function cambioCantidad(){
            $this->llenadoPost();
            $pre = '';
            if($_POST['sentido'] == "suma"){
                $pre = mysqli_prepare($this->con, "UPDATE productos SET cantidad = cantidad + ? WHERE id = ?");
            }
            elseif($_POST['sentido'] == "resta") {
                $pre = mysqli_prepare($this->con, "UPDATE productos SET cantidad = cantidad - ? WHERE id = ?");                
            }            
            $pre->bind_param("ss", $this->cantidad, $this->id);
            $pre->execute();
        }

        public static function fecha(){
            $fecha = getdate();
            return $fecha;
        }

    }


?>